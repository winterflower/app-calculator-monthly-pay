import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class CalForm extends StatefulWidget {
  const CalForm({super.key, required this.title});

  final String title;

  @override
  State<CalForm> createState() => _CalFormState();
}

class _CalFormState extends State<CalForm> {
  final _formKey = GlobalKey<FormBuilderState>();
  bool _monthlyPayHasError = false;
  bool _minusPayHasError = false;

  num _payNationalPension = 0;
  num _payHealthInsurance = 0;
  num _payLongTermCare = 0;
  num _payEmploymentInsurance = 0;
  num _payIncomeTax = 0;
  num _payLocalIncomeTax = 0;
  num _totalDeductions = 0;
  num _resultMonthlyPay = 0;

  void calculatorMonthlyPay(num originMonthlyPay, num minusPay) {
    //과세금액 계산
    num taxationPay = originMonthlyPay - minusPay; //월급액 - 비과세액 = 과세금액


    setState(() {
      _payNationalPension = (taxationPay * 0.045).ceil(); //국민연금 계산하기
      _payHealthInsurance = (taxationPay * 0.03495).ceil(); //건강보험 계산하기
      _payLongTermCare = (_payHealthInsurance * 0.1227).ceil(); //장기요양보험 계산하기
      _payEmploymentInsurance = (taxationPay * 0.009).ceil(); //고용보험 계산하기

      num taxPercent;
      if (taxationPay < 30000000) {
        taxPercent = 0.02;
      } else if (taxationPay < 60000000) {
        taxPercent = 0.04;
      } else {
        taxPercent = 0.05;
      }
      _payIncomeTax = (taxationPay * taxPercent).ceil();

      _payLocalIncomeTax = (_payIncomeTax * 0.1).ceil();
      _totalDeductions = (_payNationalPension + _payHealthInsurance + _payLongTermCare + _payEmploymentInsurance + _payIncomeTax + _payLocalIncomeTax).ceil();
      _resultMonthlyPay = (originMonthlyPay - _totalDeductions).ceil();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[
          FormBuilder(
            key: _formKey,
            onChanged: () {
              _formKey.currentState!.save();
              debugPrint(_formKey.currentState!.value.toString());
            },
            autovalidateMode: AutovalidateMode.disabled,
            initialValue: const {
              'monthlyPay' : '0',
              'minusPay' : '0'
            },
            skipDisabled: true,
            child: Column(
              children: <Widget>[
                Container(
                  child: FormBuilderTextField(
                    autovalidateMode: AutovalidateMode.always,
                    name: 'monthlyPay',
                    decoration: InputDecoration(
                      labelText: '월급액',
                      suffixIcon: _monthlyPayHasError
                          ? const Icon(Icons.error, color: Colors.red)
                          : const Icon(Icons.check, color: Colors.green),
                    ),
                    onChanged: (val) {
                      setState(() {
                        _monthlyPayHasError = !(_formKey.currentState?.fields['monthlyPay']
                            ?.validate() ??
                            false);
                      });
                    },
                    // valueTransformer: (text) => num.tryParse(text),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: "이 값은 필수입니다."),
                      FormBuilderValidators.numeric(errorText: "숫자만 입력 가능합니다."),
                      FormBuilderValidators.max(100000000, errorText: "최대 값은 1억 입니다."),
                    ]),
                    // initialValue: '12',
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.next,
                  ),
                  padding: EdgeInsets.only(left: 30, top: 20, right: 30, bottom: 20),
                ),
                Container(
                  child: FormBuilderTextField(
                    autovalidateMode: AutovalidateMode.always,
                    name: 'minusPay',
                    decoration: InputDecoration(
                      labelText: '비과세액',
                      suffixIcon: _minusPayHasError
                          ? const Icon(Icons.error, color: Colors.red)
                          : const Icon(Icons.check, color: Colors.green),
                    ),
                    onChanged: (val) {
                      setState(() {
                        _minusPayHasError = !(_formKey.currentState?.fields['minusPay']
                            ?.validate() ??
                            false);
                      });
                    },
                    // valueTransformer: (text) => num.tryParse(text),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: "이 값은 필수입니다."),
                      FormBuilderValidators.numeric(errorText: "숫자만 입력 가능합니다."),
                      FormBuilderValidators.max(10000000, errorText: "최대 값은 1천만원 입니다."),
                    ]),
                    // initialValue: '12',
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.next,
                  ),
                  padding: EdgeInsets.only(left: 30, top: 20, right: 30, bottom: 20),
                ),
                Container(
                  child: ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState?.saveAndValidate() ?? false) {
                        debugPrint(_formKey.currentState?.value.toString());
                        calculatorMonthlyPay(
                            num.parse(_formKey.currentState!.fields['monthlyPay']!.value),
                            num.parse(_formKey.currentState!.fields['minusPay']!.value)
                        );
                      }
                    },
                    child: const Text(
                      '계산하기',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  padding: EdgeInsets.fromLTRB(30, 20, 30, 20),
                ),
              ],
            ),
          ),
          Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text('국민연금'),
                Container(
                  color: Color.fromRGBO(52, 235, 143, 70),
                  child: Text(_payNationalPension.toString(),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('건강보험'),
                Container(
                  color: Color.fromRGBO(52, 235, 143, 70),
                  child: Text(_payHealthInsurance.toString(),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('장기요양'),
                Container(
                  color: Color.fromRGBO(52, 235, 143, 70),
                  child: Text(_payLongTermCare.toString(),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('고용보험'),
                Container(
                  color: Color.fromRGBO(52, 235, 143, 70),
                  child: Text(_payEmploymentInsurance.toString(),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('소득세'),
                Container(
                  color: Color.fromRGBO(52, 235, 143, 70),
                  child: Text(_payIncomeTax.toString(),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('지방소득세'),
                Container(
                  color: Color.fromRGBO(52, 235, 143, 70),
                  child: Text(_payLocalIncomeTax.toString(),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('공제액 합계'),
                Container(
                  color: Color.fromRGBO(255, 33, 216, 70),
                  child: Text(_totalDeductions.toString(),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('월 예상 실수령액'),
                Container(
                  color: Color.fromRGBO(255, 33, 216, 70),
                  child: Text(_resultMonthlyPay.toString(),
                  ),
                ),
              ],
            ),
        ],
      ),
    );
  }
}
